#pragma once

#include <iostream>;

#include <Eigen/Dense>;
#include <Eigen/LU>

#include <functional>

#include <vector>
#include <algorithm>

struct groupUs
{
	groupUs() : mI(0), mJ(0), mK(0), mValue(0.0f) {}
	groupUs(int pI, int pJ, int pK, int pValue)
	{
		mI = pI;
		mJ = pJ;
		mK = pK;
		mValue = pValue;
	}
	int mI, mJ, mK;
	float mValue;

	float distanceSquared(int pI, int pJ, int pK) 
	{
		return (mI - pI) * (mI - pI) + (mJ - pJ) * (mJ - pJ) + (mK - pK) * (mK - pK);
	}

	bool operator<(const groupUs& rhs) const
	{
		return mI * mI + mJ * mJ + mK * mK < rhs.mI * rhs.mI + rhs.mJ * rhs.mJ + rhs.mK * rhs.mK;
	}
};

class DataSet 
{
public:
	DataSet(int pSizeX, int pSizeY, int pSizeZ, float pPercentage);
	DataSet(std::function< float(int, int, int) > pEvaluationFunction);
	DataSet(const char* pFilename);

	void setColored1DSlicePointer(int mMode, int mSliceNumber);
	inline void setMode(int pMode) { mMode = pMode; }
	inline float* getTexturePointer() { return mTexturePointer; }

	~DataSet();

private:

	float*** mDataArray;
	float*** mPartialDataArray;
	
	float*** mShepardGlobal;
	float*** mHardyGlobal;

	float*** mShepardLocal;
	float*** mHardyLocal;

	float* mTexturePointer;

	int mSizeX;
	int mSizeY;
	int mSizeZ;

	float mMaxValue;
	float mMinValue;

	int mMode;

	float** getSliceArray(int mNumber);

	void renconstructPartialDataSet();

	void shepardGlobal();
	void hardyGlobal();

	void shepardLocal();
	void hardyLocal();

	std::vector<groupUs> findKNN(int pK, int i, int j, int k);

	inline float computeDistSquared(int i, int j, int k, int ii, int jj, int kk) 
	{
		return pow(ii - i, 2) + pow(jj - j, 2) + pow(kk - k, 2);
	}

};