#include "DataSet.h"
#include "global.h"

#include <assert.h>
#include <math.h>

#include <omp.h>
#include <ppl.h>
#include <stdio.h>

#include <map>


using Eigen::MatrixXd;
using Eigen::VectorXd;


DataSet::DataSet(int pSizeX, int pSizeY, int pSizeZ, float pPercentage)
{
	mSizeX = global::gDataSetSize;
	mSizeY = global::gDataSetSize;
	mSizeZ = global::gDataSetSize;

	mTexturePointer = (float*)malloc(3 * sizeof(float) * global::gDataSetSize * global::gDataSetSize);

	bool first = true;

	mDataArray = new float**[mSizeX];
	mPartialDataArray = new float**[mSizeX];
	mShepardGlobal = new float**[mSizeX];
	mHardyGlobal = new float**[mSizeX];
	mShepardLocal = new float**[mSizeX];
	mHardyLocal = new float**[mSizeX];

	#pragma omp parallel for
	for (int i = 0; i < mSizeX; ++i)
	{
		mDataArray[i] = new float*[mSizeY];
		mPartialDataArray[i] = new float*[mSizeY];
		mShepardGlobal[i] = new float*[mSizeY];
		mHardyGlobal[i] = new float*[mSizeX];
		mShepardLocal[i] = new float*[mSizeX];
		mHardyLocal[i] = new float*[mSizeX];

		#pragma omp parallel for
		for (int j = 0; j < mSizeY; ++j)
		{
			mDataArray[i][j] = new float[mSizeZ];
			mPartialDataArray[i][j] = new float[mSizeZ];
			mShepardGlobal[i][j] = new float[mSizeZ];
			mHardyGlobal[i][j] = new float[mSizeZ];
			mShepardLocal[i][j] = new float[mSizeZ];
			mHardyLocal[i][j] = new float[mSizeZ];

			#pragma omp parallel for
			for (int k = 0; k < mSizeZ; ++k)
			{
				
				mDataArray[i][j][k] = i*i + j*j + k*k;

				// initalize these 2 to -1.0
				mPartialDataArray[i][j][k] = -1.0f;
				mShepardGlobal[i][j][k] = -1.0f;
				mHardyGlobal[i][j][k] = -1.0f;
				mShepardLocal[i][j][k] = -1.0f;
				mHardyLocal[i][j][k] = -1.0f;

				// update max and min value of the dataset
				if (first)
				{
					mMaxValue = mDataArray[i][j][k];
					mMinValue = mDataArray[i][j][k];
					first = false;
				}
				else
				{
					if (mDataArray[i][j][k] > mMaxValue)
					{
						mMaxValue = mDataArray[i][j][k];
					}
					else if (mDataArray[i][j][k] < mMinValue)
					{
						mMinValue = mDataArray[i][j][k];
					}
				}
			}
		}
	}
	assert(mMinValue < mMaxValue);

	int lPartialNumber = floor(pPercentage * pSizeY * pSizeZ);

	#pragma omp parallel
	{
		srand(int(time(NULL)) ^ omp_get_thread_num());
		#pragma omp parallel for
		for (auto k = 0; k < mSizeZ; k++) 
		{
			#pragma omp parallel for
			for (auto i = 0; i < lPartialNumber; i++)
			{
				int lX = rand() % global::gDataSetSize;
				int lY = rand() % global::gDataSetSize;

				float lValue = lX * lX + lY * lY + k * k;
				mPartialDataArray[lX][lY][k] = lValue;
			}
		}
	}
	renconstructPartialDataSet();

}

DataSet::DataSet(std::function<float(int, int, int)> pEvaluationFunction)
{

}

DataSet::DataSet(const char *pFilename) {}

void DataSet::setColored1DSlicePointer(int pMode, int pSliceNumber)
{
	float** l2DNonColoredArray = getSliceArray(pSliceNumber);

	int lSizeOne;
	int lSizeTwo;

	lSizeOne = mSizeX;
	lSizeTwo = mSizeY;

	float lMax = 0.0;
	float lMin = 999999999.0;

	#pragma omp parallel for
	for (int i = 0; i < mSizeX; i++) 
	{
		#pragma omp parallel for
		for (int j = 0; j < mSizeY; j++) 
		{
			if (mDataArray[i][j][global::gSliceIndex] > lMax)
			{
				lMax = mDataArray[i][j][global::gSliceIndex];
			}
			else if (mDataArray[i][j][global::gSliceIndex] < lMin && mDataArray >= 0)
			{
				lMin = mDataArray[i][j][global::gSliceIndex];
			}
		}
	}

	// flatten 2D array into 1D array
	// width first for OpenGL texture support
	#pragma omp parallel for
	for (int i = 0; i < lSizeOne; i++) 
	{
		#pragma omp parallel for
		for (int j = 0; j < lSizeTwo; j++) 
		{	
			auto lInterval = mMaxValue - mMinValue;
			auto lOnePiece = lInterval / 60;

			auto lHelper = l2DNonColoredArray[i][j] / lOnePiece;
			auto lClose = (int)ceil(lHelper);
			if ( lClose % 2 == 0)
			{
				int l1DIndex = 3 * (j % lSizeOne + i * lSizeTwo);
				mTexturePointer[l1DIndex] = ((l2DNonColoredArray[i][j] - lMin) / (lMax - lMin)) * 1.0f;
				mTexturePointer[l1DIndex + 1] = ((l2DNonColoredArray[i][j] - lMin) / (lMax - lMin)) * 1.0f;
				mTexturePointer[l1DIndex + 2] = ((l2DNonColoredArray[i][j] - lMin) / (lMax - lMin)) * 1.0f;
			}
		}
	}
}

float** DataSet::getSliceArray(int pSliceNumber)
{
	if (mMode == 0) 
	{
		return mDataArray[pSliceNumber];
	}
	else if (mMode == 1) 
	{
		return mPartialDataArray[pSliceNumber];
	}
	else if (mMode == 2)
	{
		return mShepardGlobal[pSliceNumber];
	}
	else if (mMode == 3) 
	{
		return mHardyGlobal[pSliceNumber];
	}
	else if (mMode == 4)
	{
		return mShepardLocal[pSliceNumber];
	}
	else if (mMode == 5)
	{
		return mHardyLocal[pSliceNumber];
	}
	else 
	{
		assert(1 == 0);
		return mDataArray[pSliceNumber];
	}
}

void DataSet::renconstructPartialDataSet()
{
	//shepardGlobal();
	//shepardLocal();

	//hardyGlobal();
	hardyLocal();
}

void DataSet::shepardGlobal()
{
	int i = global::gSliceIndex;

    #pragma omp parallel for
	for (int j = 0; j < global::gDataSetSize; j++) 
	{
		for (int k = 0; k < global::gDataSetSize; k++)
		{
			// check if this value is missing and needs to be interpolated
			if (mPartialDataArray[i][j][k] < 0.0f) 
			{
				// yes, we need to interpolate this value
				auto lInverseDistSquared = 0.0f;
				auto lValuedInverseDistSquared = 0.0f;

				#pragma omp parallel for
				for (int ii = 0; ii < mSizeX; ii++)
				{
					#pragma omp parallel for
					for (int jj = 0; jj < mSizeY; jj++)
					{
						#pragma omp parallel for
						for (int kk = 0; kk < mSizeZ; kk++)
						{
							if (mPartialDataArray[ii][jj][kk] >= 0.0f)
							{
								if (i == ii && j == jj && k == kk) 
								{
									assert(1 == 0);
								}
								// this value is known, use it to interpolate
								float lInverseDistSquaredLocal = 1.0f / (float)((i - ii) * (i - ii) + (j - jj) * (j - jj) + (k - kk) * (k - kk));
								assert(lInverseDistSquaredLocal != 0);
								lInverseDistSquared += lInverseDistSquaredLocal;
								lValuedInverseDistSquared += lInverseDistSquaredLocal * mPartialDataArray[ii][jj][kk];
							}
						}
					}
				}
				mShepardGlobal[i][j][k] = lValuedInverseDistSquared / lInverseDistSquared;
				assert(lInverseDistSquared != 0);
				assert(mShepardGlobal[i][j][k] >= mMinValue);
				assert(mShepardGlobal[i][j][k] <= mMaxValue);
			}
			else 
			{
				// this value isnt missing, just copy it
				mShepardGlobal[i][j][k] = mPartialDataArray[i][j][k];
			}
		}
	}


	// this is just for debugging purposes
	#pragma omp parallel for
	for (int j = 0; j < mSizeY; j++)
	{
		#pragma omp parallel for
		for (int k = 0; k < mSizeZ; k++)
		{
			assert(mShepardGlobal[i][j][k] >= mMinValue);
			assert(mShepardGlobal[i][j][k] <= mMaxValue);
		}
	}
}

void DataSet::hardyGlobal()
{
	std::map<int, groupUs> lMap;
	int lCounter = 0;

	// fills a hashmap hashing a key based on idecis to the value
	for (int i = 0; i < global::gDataSetSize; i++)
	{
		
		for (int j = 0; j < global::gDataSetSize; j++)
		{
			
			for (int k = 0; k < global::gDataSetSize; k++)
			{
				if (mPartialDataArray[i][j][k] >= 0.0f) 
				{
					lMap[lCounter] = groupUs(i, j, k, mPartialDataArray[i][j][k]);
					lCounter++;
				}
			}
		}
	}

	assert(lCounter > 0);
	assert(lCounter == lMap.size());
	MatrixXd lHugeMatrix(lCounter, lCounter);

	VectorXd lFValues(lCounter);

	int lColumnIndex = 0;
	int lRowIndex = 0;

	// fill the matrix	

	//#pragma omp paralell for
	for (std::map<int, groupUs>::iterator it = lMap.begin(); it != lMap.end(); it++)
	{
		lColumnIndex = 0;
		//#pragma omp paralell for
		for (std::map<int, groupUs>::iterator itNested = lMap.begin(); itNested != lMap.end(); itNested++)
		{
			float dist = computeDistSquared(it->second.mI, it->second.mJ, it->second.mK, itNested->second.mI, itNested->second.mJ, itNested->second.mK);
			lHugeMatrix(lRowIndex, lColumnIndex) = sqrt(global::gRSquared + dist);
			assert(lHugeMatrix(lRowIndex, lColumnIndex) >= sqrt(global::gRSquared));
			lColumnIndex++;
		}
		lFValues(lRowIndex) = it->second.mValue;
		assert(lFValues(lRowIndex) >= 0.0f);
		lRowIndex++;
	}

	// OUCH!!!
	VectorXd lCValues(lFValues.size());
	lCValues = lHugeMatrix.inverse() * lFValues;

	int debugging = 0;

	for (int i = 0; i < lCValues.size(); i++) 
	{
		if (isinf(lCValues(i))) 
		{
			debugging++;
		}
	}



	int lDistanceHelper;

	// compute the missing values
	for (int i = 0; i < global::gDataSetSize; i++)
	{

		for (int j = 0; j < global::gDataSetSize; j++)
		{

			for (int k = 0; k < global::gDataSetSize; k++)
			{
				if (mPartialDataArray[i][j][k] < 0.0f)
				{
					
					float newFValue = 0.0f;
					for (std::map<int, groupUs>::iterator it = lMap.begin(); it != lMap.end(); it++)
					{
						lDistanceHelper = std::distance(lMap.begin(), it);
						//assert(lCValues[lDistanceHelper]);
						newFValue += lCValues[lDistanceHelper] * sqrt(global::gRSquared + computeDistSquared(i, j, k, it->second.mI, it->second.mJ, it->second.mK));
					}
					//assert(newFValue >= mMinValue);
					//assert(newFValue <= mMaxValue);
					mHardyGlobal[i][j][k] = newFValue;
				}
			}
		}
	}
}

void DataSet::shepardLocal()
{
	int i = global::gSliceIndex;

	#pragma omp parallel for
	for (int j = 0; j < global::gDataSetSize; j++)
	{
		for (int k = 0; k < global::gDataSetSize; k++)
		{
			// check if this value is missing and needs to be interpolated
			if (mPartialDataArray[i][j][k] < 0.0f)
			{
				// compute KNN, the slow way
				std::vector<groupUs> lClosestSites = findKNN(global::gK, i, j, k);

				// yes, we need to interpolate this value
				auto lInverseDistSquared = 0.0f;
				auto lValuedInverseDistSquared = 0.0f;

				for (auto const& elem : lClosestSites)
				{
					float lInverseDistSquaredLocal = 1.0f / (float)((i - elem.mI) * (i - elem.mI) + (j - elem.mJ) * (j - elem.mJ) + (k - elem.mK) * (k - elem.mK));
					assert(lInverseDistSquaredLocal != 0);
					lInverseDistSquared += lInverseDistSquaredLocal;
					lValuedInverseDistSquared += lInverseDistSquaredLocal * elem.mValue;
				}
				mShepardLocal[i][j][k] = lValuedInverseDistSquared / lInverseDistSquared;
				assert(lInverseDistSquared != 0);
				assert(mShepardLocal[i][j][k] >= mMinValue);
				assert(mShepardLocal[i][j][k] <= mMaxValue);
			}
			else
			{
				// this value isnt missing, just copy it
				mShepardLocal[i][j][k] = mPartialDataArray[i][j][k];
			}
		}
	}
}

void DataSet::hardyLocal()
{
	// compute the missing values
	#pragma omp paralell for
	for (int i = 0; i < global::gDataSetSize; i++)
	{
		for (int j = 0; j < global::gDataSetSize; j++)
		{
			for (int k = 0; k < global::gDataSetSize; k++)
			{
				// check if this value is missing 
				if (mPartialDataArray[i][j][k] < 0.0f)
				{
					// it is missing! find the KNN
					std::vector<groupUs> lClosestSites = findKNN(global::gK, i, j, k);

					int lColumnIndex = 0;
					int lRowIndex = 0;

					// fill the matrix
					// AHA!!!
					// use the true size of the KNN vector as the size of the matrix
					MatrixXd lLocalMatrix(lClosestSites.size(), lClosestSites.size());
					VectorXd lFValues(lClosestSites.size());

					//#pragma omp paralell for
					for (auto &lCurr : lClosestSites)
					{
						lColumnIndex = 0;

						//#pragma omp paralell for
						for (auto &lNested : lClosestSites)
						{
							float dist = computeDistSquared(lCurr.mI, lCurr.mJ, lCurr.mK, lNested.mI, lNested.mJ, lNested.mK);
							lLocalMatrix(lRowIndex, lColumnIndex) = sqrt(global::gRSquared + dist);
							assert(lLocalMatrix(lRowIndex, lColumnIndex) >= sqrt(global::gRSquared));
							lColumnIndex++;
						}
						lFValues(lRowIndex) = lCurr.mValue;
						assert(lFValues(lRowIndex) >= 0.0f);
						lRowIndex++;
					} // matrix and fvalues have been filled for all KNNs

					// compute the inverse
					VectorXd lCValues(lFValues.size());
					lCValues = lLocalMatrix.inverse() * lFValues;

					float newFValue = 0.0f;
					for (int i = 0; i < lFValues.size() - 1; i++)
					{
						//assert(lCValues[lDistanceHelper]);
						newFValue += lCValues[i] * sqrt(global::gRSquared + computeDistSquared(i, j, k, lClosestSites.at(i).mI, lClosestSites.at(i).mJ, lClosestSites.at(i).mK));
					}
					//assert(newFValue >= mMinValue);
					//assert(newFValue <= mMaxValue);
					mHardyLocal[i][j][k] = newFValue;
				}
				else 
				{
					mHardyLocal[i][j][k] = mPartialDataArray[i][j][k];
				}
			}
		}
	}
}

std::vector<groupUs> DataSet::findKNN(int pK, int i, int j, int k)
{
	std::vector<groupUs> lToBeReturned;

	for (int ii = 0; ii < global::gDataSetSize; ii++) 
	{
		for (int jj = 0; jj < global::gDataSetSize; jj++) 
		{
			for (int kk = 0; kk < global::gDataSetSize; kk++)
			{
				if (ii == i && jj == j && kk == k) 
				{
					// this is the point we are searching the neighbors to
					continue;
				}
				if (mPartialDataArray[ii][jj][kk] < 0.0f) 
				{
					// we are not at a site
					continue;
				}

				// we are at a site and not at the requested point

				// the vector is not yet filled up 
				if (lToBeReturned.size() != pK)
				{
					lToBeReturned.push_back(groupUs(ii, jj, kk, mDataArray[ii][jj][kk]));
					std::sort(lToBeReturned.begin(), lToBeReturned.end());
				}
				else
				{
					// the vector is sorted and full
					float distSquared = (ii - i) * (ii - i) + (jj - j) * (jj - j) + (kk - k) * (kk - k);
					if (lToBeReturned.at(pK - 1).distanceSquared(i, j, k) > distSquared)
					{
						// this point is closer than the furthest point -> put it in there at the very last spot
						// we need to sort again
						lToBeReturned.at(pK - 1) = groupUs(ii, jj, kk, mDataArray[ii][jj][kk]);
						std::sort(lToBeReturned.begin(), lToBeReturned.end());
					}
				}
			}
		}
	}

	return lToBeReturned;
}

DataSet::~DataSet()
{
	for (int i = 0; i < mSizeX; i++)
	{
		for (int j = 0; j < mSizeY; j++)
		{
			//delete[] mDataArray[i][j];
			//delete[] mPartialDataArray[i][j];
		}
		delete[] mDataArray[i];
		delete[] mPartialDataArray[i];
	}
	delete[] mDataArray;
	delete[] mPartialDataArray;

	delete[] mTexturePointer;
}
