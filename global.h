#pragma once

namespace global
{
	extern int gScreenHeight;
	extern int gScreenWidth;

	extern int gDataSetSize;
	extern int gSliceIndex;

	extern float gRSquared;

	extern int gK;
}