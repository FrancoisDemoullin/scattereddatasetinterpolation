#include <iostream>
#include <string>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "global.h"
#include "SliceRenderer.h"
#include "DataSet.h"

#include <glm/gtc/type_ptr.hpp>

#include <Eigen/Dense>

using Eigen::MatrixXd;

//using namespace std;
using namespace global;

int global::gScreenHeight = 600;
int global::gScreenWidth = 600;

int global::gDataSetSize = 10;
int global::gSliceIndex = 5;

float global::gRSquared = 1.0;

int global::gK = 10;

/*
User input:
Function prototypes
Keys array allows multiple keyes to be pressed simultaneously
*/

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void executeInput();
bool keys[1024];

SliceRenderer* gSliceRenderer;
DataSet* gDataSet;

int main(int argc, char *argv[])
{	
	// Init GLFW
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	GLFWwindow* window = glfwCreateWindow(global::gScreenWidth, global::gScreenHeight, "ECS277 - A2", nullptr, nullptr);
	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);

	// Options
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Initialize GLEW to setup the OpenGL Function pointers
	glewExperimental = GL_TRUE;
	glewInit();

	// Define the viewport dimensions
	glViewport(0, 0, global::gScreenHeight, global::gScreenWidth);

	// Setup some OpenGL options
	glEnable(GL_DEPTH_TEST);

	gSliceRenderer = new SliceRenderer();
	gDataSet = new DataSet(global::gDataSetSize, global::gDataSetSize, global::gDataSetSize, 0.1);

	gDataSet->setMode(0);

	bool first = true;

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
	glfwPollEvents();
	executeInput();

	// Clear the colorbuffer
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// sets the pointer for the texture
	gDataSet->setColored1DSlicePointer(0, global::gSliceIndex);

	float* lPassAround = gDataSet->getTexturePointer();

	gSliceRenderer->updateTexture(lPassAround);
	gSliceRenderer->renderTexture();

	// Swap the buffers
	glfwSwapBuffers(window);

	//cout << "Screen" << endl;
	}

	glfwTerminate();

	//delete keys;
	//gSliceRenderer->~SliceRenderer();
	//delete gSliceRenderer;
	//gDataSet->~DataSet();
	//delete gDataSet;

	/*
	_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDOUT);
	_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
	_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDOUT);

	_CrtDumpMemoryLeaks();
	*/

	return 0;

}

// executes the user input
void executeInput()
{
	if (keys[GLFW_KEY_0])
	{
		gDataSet->setMode(0);
	}
	if (keys[GLFW_KEY_1])
	{
		gDataSet->setMode(1);
	}
	if (keys[GLFW_KEY_2])
	{
		gDataSet->setMode(2);
	}
	if (keys[GLFW_KEY_3])
	{
		gDataSet->setMode(3);
	}
	if (keys[GLFW_KEY_4]) 
	{
		gDataSet->setMode(4);
	}
	if (keys[GLFW_KEY_5])
	{
		gDataSet->setMode(5);
	}
}

// this is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	if (key >= 0 && key < 1024)
	{
		if (action == GLFW_PRESS)
		{
			keys[key] = true;
		}
		else if (action == GLFW_RELEASE)
		{
			keys[key] = false;
		}
	}
}

