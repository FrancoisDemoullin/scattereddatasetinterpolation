# README #

Scattered data interpolation using Shepard's and Hardy's method using C++, OpenGL and GLSL (and OpenMP if you are looking for performance optimization)
Francois Demoullin

Find a short presentation with plenty of pictures and all the technical information behind the interpolation method at: [http://1drv.ms/1TpAzoV](http://1drv.ms/1TpAzoV)

### What is this repository for? ###

* This is a fully working Scattered data set interpolation program with support for Hardy's local, Hardy's global, Shepard's local and Shepard's global method.
* Pass in a function, my program will compute a complete data set. Then omit a user defined percentage of points and then reconstruct said data set using interpolations and 4 different methods
* The images produced depend on plenty of parameters which are explined below. Results vary based on method used, data set and on the ideal combination of the data set and  the right parameters. 


### How do I get set up? ###

Clone the repository using SourceTree onto your local machine. 
The source files come with a Microsoft Visual Studio 2015 solution file. Open the solution using Visual Studio.
Grab the external dependency file from Source Tree. This file contains all the necessary libraries and includes files. The libraries were built for a 64bit windows machine >and for all libraries the target platform is VS2015. If you are not running 64bit windows you might have to rebuild the libraries or resort to the pre-build binaries. >Information about all the necessary libraries can be found below.

Place the "Dependencies" folder such that "../../Dependencies" is accessible from Main.cpp. 
This means that you should have a folder structure similar to the following
>---------ProjectFolder  
>---------------------Dependencies  
>-------------------------------- include  
>-------------------------------- lib  
>---------------------SourceCode  
>-----------------------------ParticleSystem  
>-------------------------------------------Main.cpp  
>-------------------------------------------.cpp and .h files  
>-----------------------------VSSolutionFile 

I chose this approach of structuring my projects because I can have multiple projects referencing the dependencies using an absolute path.

### What are the parameters I can change ###

There are many parameters you can change. You will have to change them in the code however, there is no UI  for this project.

1. The size of the data set. Change line 24 of main.cpp
The data set has to be a cube, you can only specify one single size, all directions will have the same size.

2. The index of the slice that is to be rendered on the screen. Change line 25 of main.java.
The slice index sets x to the slice index. So what is being displayed is the one slice where x == gSliceIndex. 
It is important that the sliceIndex is never bigger than the size of the data set. 

3. Screen Height and Screen Width. Change line 21 and 22 of main.cpp

4. The percentage of omitted points in the data set. Main.cpp line 74, the last parameter to the function call.

5. K. How many nearest neighbors you would like to interpolate for the local methods. change line 27 of main.cpp

### Who do I talk to? ###

I know this project is not the most worked out thing in the world.
However if you did like it, please drop me a note.

Francois Demoullin
f.demoullin@gmail.com