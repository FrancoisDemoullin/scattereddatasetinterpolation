#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Shader
{
public:
	GLuint mProgram;

	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);

	// copy constructor
	Shader(const Shader& pOther);

	// Uses the current shader
	void use();
};